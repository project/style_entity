<?php

namespace Drupal\style_entity;

use Drupal\Core\Entity\EntityViewBuilder;

/**
 * Provides a Styles view builder.
 */
class StylesViewBuilder extends EntityViewBuilder {

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

}
