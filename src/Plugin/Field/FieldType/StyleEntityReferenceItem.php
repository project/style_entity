<?php

namespace Drupal\style_entity\Plugin\Field\FieldType;

use Drupal\Core\Field\Plugin\Field\FieldType\EntityReferenceItem;

/**
 * Defines the 'style_entity_reference' entity field type.
 *
 * @FieldType(
 *   id = "style_entity_reference",
 *   label = @Translation("Style Entity reference"),
 *   description = @Translation("An entity field containing an entity reference."),
 *   category = @Translation("Reference"),
 *   no_ui = TRUE,
 *   default_widget = "entity_reference_autocomplete",
 *   default_formatter = "style_entity_reference_label",
 *   list_class = "\Drupal\Core\Field\EntityReferenceFieldItemList",
 * )
 *
 * @deprecated in style_entity:2.0 and is removed in style_entity:3.0.
 *   Use entity_reference FieldType instead.
 *
 * @see https://www.drupal.org/project/style_entity/issues/3302083
 */
class StyleEntityReferenceItem extends EntityReferenceItem {}
