<?php

namespace Drupal\style_entity\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Defines the Styles entity.
 *
 * @ConfigEntityType(
 *   id = "styles",
 *   label = @Translation("Styles"),
 *   handlers = {
 *     "view_builder" = "Drupal\style_entity\StylesViewBuilder",
 *     "list_builder" = "Drupal\style_entity\StylesListBuilder",
 *     "form" = {
 *       "add" = "Drupal\style_entity\Form\StylesForm",
 *       "edit" = "Drupal\style_entity\Form\StylesForm",
 *       "delete" = "Drupal\style_entity\Form\StylesDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\style_entity\StylesHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix = "styles",
 *   admin_permission = "administer styles",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   common_reference_target = TRUE,
 *   links = {
 *     "canonical" = "/admin/structure/styles/{styles}",
 *     "add-form" = "/admin/structure/styles/add",
 *     "edit-form" = "/admin/structure/styles/{styles}/edit",
 *     "delete-form" = "/admin/structure/styles/{styles}/delete",
 *     "collection" = "/admin/structure/styles"
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "classes",
 *     "uses"
 *   }
 * )
 */
class Styles extends ConfigEntityBase implements ConfigEntityInterface {

  /**
   * The Styles ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The Styles label.
   *
   * @var string
   */
  protected $label;

  /**
   * The Classes field.
   *
   * @var array
   */
  protected $classes = [];

  /**
   * The Uses.
   *
   * @var array
   */
  protected $uses = [];

  /**
   * Get uses from configuration.
   *
   * This should not be confused with Styles::get('uses'), which would
   * get the `uses` set for a specific Styles entity. Instead,
   * Styles::getUses() returns the entire list of available uses from
   * configuration.
   */
  public static function getUses() {
    /** @var \Drupal\Core\Config\ImmutableConfig $uses */
    $use_config = \Drupal::config('style_entity.uses')->get('uses');
    if (empty($use_config)) {
      return [];
    }
    $use_config = explode("\r\n", $use_config);

    $uses = [];
    foreach ($use_config as $use) {
      $parts = explode("|", $use);
      $uses[$parts[0]] = $parts[1] ?? $parts[0];
    }
    return $uses;
  }

  /**
   * Get a list of uses enabled for this style.
   */
  public function listUses() {
    $used_uses = $this->get('uses');
    $uses = $this->getUses();
    $used = [];
    foreach ($used_uses as $key => $value) {
      if ($value && !empty($uses[$key])) {
        $used[] = $uses[$key];
      }
    }
    return implode(', ', $used);
  }

}
