<?php

namespace Drupal\style_entity\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Defines a form that configures forms module settings.
 */
class UsesForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'style_entity_uses';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'style_entity.uses',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('style_entity.uses');
    $form['uses'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Uses'),
      '#description' => $this->t('Add use definitions using a format of key|Label, with each definition on a new line.'),
      '#default_value' => $config->get('uses'),
      '#rows' => 10,
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $this->config('style_entity.uses')
      ->set('uses', $values['uses'])
      ->save();
    parent::submitForm($form, $form_state);
  }

}
