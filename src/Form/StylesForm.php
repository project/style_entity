<?php

namespace Drupal\style_entity\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class constructs StylesForm.
 */
class StylesForm extends EntityForm {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);
    $styles = $this->entity;
    $classes = $styles->get('classes') ?? [];

    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $styles->label(),
      '#description' => $this->t("Label for the Styles."),
      '#required' => TRUE,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $styles->id(),
      '#machine_name' => [
        'exists' => '\Drupal\style_entity\Entity\Styles::load',
      ],
      '#disabled' => !$styles->isNew(),
    ];

    $form['classes'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Classes'),
      '#default_value' => implode(' ', $classes),
      '#description' => $this->t('Add one or more space-delimited classes.'),
      '#attributes' => [
        'id' => 'classes',
      ],
    ];

    $options = $styles->getUses();

    $form['uses'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Uses'),
      '#options' => $options,
      '#default_value' => $styles->get('uses'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $styles = $this->entity;
    $status = $styles->save();

    switch ($status) {
      case SAVED_NEW:
        $this->messenger()->addStatus($this->t('Created the %label Styles.', [
          '%label' => $styles->label(),
        ]));
        break;

      default:
        $this->messenger()->addStatus($this->t('Saved the %label Styles.', [
          '%label' => $styles->label(),
        ]));
    }
    $form_state->setRedirectUrl($styles->toUrl('collection'));
  }

  /**
   * {@inheritdoc}
   */
  protected function copyFormValuesToEntity(EntityInterface $entity, array $form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    foreach ($values as $key => $value) {
      if ($key === 'classes') {
        $entity->set('classes', explode(' ', $value));
      }
      else {
        $entity->set($key, $value);
      }
    }
  }

}
