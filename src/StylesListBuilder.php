<?php

namespace Drupal\style_entity;

use Drupal\Core\Config\Entity\ConfigEntityListBuilder;
use Drupal\Core\Entity\EntityInterface;

/**
 * Provides a listing of Styles entities.
 */
class StylesListBuilder extends ConfigEntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['label'] = $this->t('Styles');
    $header['id'] = $this->t('Machine name');
    $header['uses'] = $this->t('Uses');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /** @var \Drupal\style_entity\Entity\Styles $entity */
    $row['label'] = $entity->label();
    $row['id'] = $entity->id();
    $row['uses'] = $entity->listUses();
    // You probably want a few more properties here...
    return $row + parent::buildRow($entity);
  }

}
