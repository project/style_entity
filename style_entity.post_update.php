<?php

/**
 * @file
 * Post-update functions style_entity module.
 */

use Drupal\Core\Config\Entity\ConfigEntityUpdater;
use Drupal\Core\Entity\Display\EntityDisplayInterface;
use Drupal\field\FieldConfigInterface;

/**
 * Update field_storage to use entity_reference instead of
 * style_entity_reference.
 */
function style_entity_post_update_style_entity_reference_storage(&$sandbox) {
  // Changing field_storage_config type is generally not allowed by
  // the config system, so we do the bad thing and update the db directly.
  // In our case, we know the schema is identical so it's ok.
  drupal_flush_all_caches();
  $style_fields = \Drupal::service('entity_type.manager')
    ->getStorage('field_storage_config')
    ->loadByProperties(['type' => 'style_entity_reference']);
  foreach ($style_fields as $field) {
    $data = \Drupal::database()->select('config', 'c')
      ->fields('c', ['data'])
      ->condition('name', 'field.storage.' . $field->id())
      ->execute();
    $value = $data->fetchAll()[0]->data ?? NULL;
    $value = str_replace('"type";s:22:"style_entity_reference"', '"type";s:16:"entity_reference"', $value);
    $value = str_replace('"module";s:12:"style_entity"', '"module";s:4:"core"', $value);
    \Drupal::database()->update('config')
      ->fields(['data' => $value])
      ->condition('name', 'field.storage.' . $field->id())
      ->execute();
    \Drupal::logger('style_entity')->notice('field.storage.' . $field->id() . ' updated to be entity_reference field.');
  }
}

/**
 * Update entity view display to use entity_reference_label instead of
 * style_entity_reference_label.
 */
function style_entity_post_update_style_entity_reference_formatter(&$sandbox) {
  $config_entity_updater = \Drupal::classResolver(ConfigEntityUpdater::class);
  $formatter_callback = function (EntityDisplayInterface $display) {
    $needs_save = FALSE;
    foreach ($display->getComponents() as $field_name => $component) {
      if (isset($component['type'])) {
        if ($component['type'] === 'style_entity_reference_label') {
          $component['type'] = 'entity_reference_label';
          $display->setComponent($field_name, $component);
          \Drupal::logger('style_entity')->notice($display->id() . ' updated to use entity_reference_label formatter.');
          $needs_save = TRUE;
        }
      }
    }
    return $needs_save;
  };
  $config_entity_updater->update($sandbox, 'entity_view_display', $formatter_callback);
}

/**
 * Update field_config to use entity_reference instead of
 * style_entity_reference.
 */
function style_entity_post_update_style_entity_reference_field(&$sandbox) {
  $config_entity_updater = \Drupal::classResolver(ConfigEntityUpdater::class);
  // Change the field_type to entity_reference.
  $field_callback = function (FieldConfigInterface $field) {
    if ($field->get('field_type') === 'style_entity_reference') {
      $field->set('field_type', 'entity_reference');
      \Drupal::logger('style_entity')->notice($field->id() . ' updated to use entity_reference field_type.');
      return TRUE;
    }
  };
  $config_entity_updater->update($sandbox, 'field_config', $field_callback);
}
