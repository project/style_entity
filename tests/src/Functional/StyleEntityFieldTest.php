<?php

namespace Drupal\Tests\style_entity\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Functional test(s) for style_entity field.
 *
 * @group style_entity
 */
class StyleEntityFieldTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'style_entity_test',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * Creates a test node with a style and checks that the classes are output.
   */
  public function testStyleEntityClass() {
    // Create a test node using the style from the test module.
    $node = \Drupal::service('entity_type.manager')->getStorage('node')->create([
      'type' => 'test_node_type',
      'title' => 'My Test Node',
      'field_style' => ['target_id' => 'test_style'],
    ]);
    $node->save();

    $this->drupalGet('node/' . $node->id());
    $this->assertSession()->pageTextContains('My Test Node');
    $this->assertSession()->elementExists('css', 'article.class-from-test-style.another-class');

    // Update the style entity and revisit the node to validate caching.
    $style = \Drupal::service('entity_type.manager')->getStorage('styles')->load('test_style');
    $style->set('classes', ['different-class-this-time']);
    $style->save();

    $this->drupalGet('node/' . $node->id());
    $this->assertSession()->pageTextContains('My Test Node');
    $this->assertSession()->elementNotExists('css', 'article.class-from-test-style.another-class');
    $this->assertSession()->elementExists('css', 'article.different-class-this-time');
  }

}
