<?php

namespace Drupal\Tests\style_entity\Functional;

use Drupal\style_entity\Entity\Styles;
use Drupal\Tests\BrowserTestBase;

/**
 * Functional test(s) for style_entity form.
 *
 * @group style_entity
 */
class StyleEntityFormTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'style_entity_test',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * Test that the form works as expected.
   */
  public function testStyleEntityForm() {
    $this->drupalLogin($this->createUser(['administer styles'], 'styles_pro'));
    $page = $this->getSession()->getPage();

    // Go to the styles listing.
    $this->drupalGet('admin/structure/styles');
    $this->assertSession()->pageTextContains('Test Style');
    $this->assertSession()->pageTextContains('test_style');

    // Edit the existing style.
    $this->drupalGet('admin/structure/styles/test_style/edit');
    $this->assertSame('class-from-test-style another-class', $page->find('css', '[data-drupal-selector="edit-classes"]')->getValue());

    // Add another class and save.
    $this->submitForm(['classes' => 'different classes'], 'Save');

    // Check that listing page looks ok.
    $this->assertSession()->pageTextContains('Test Style');
    $this->assertSession()->pageTextContains('test_style');
    $this->assertSession()->statusMessageContains('Saved the Test Style Styles.');

    // Confirm that the new class was saved correctly.
    $classes = Styles::load('test_style')->get('classes');
    $this->assertSame(['different', 'classes'], $classes);

    // Add a couple three uses.
    $configuration = "my_key|My Label\r\nanother_key\r\nthird|3_3_3";
    $config = \Drupal::configFactory()->getEditable('style_entity.uses');
    $config->set('uses', $configuration);
    $config->save();

    // Add a new Styles entity.
    $this->drupalGet('admin/structure/styles/add');
    $this->submitForm([
      'label' => 'My New Style',
      'id' => 'my_new_style',
      'classes' => 'please-please',
      'uses[another_key]' => 'another_key',
    ], 'Save');

    $this->assertSession()->pageTextContains('My New Style');
    $this->assertSession()->pageTextContains('my_new_style');
    $this->assertSession()->statusMessageContains('Created the My New Style Styles.');
    $style = Styles::load('my_new_style');
    $classes = $style->get('classes');
    $this->assertSame(['please-please'], $classes);
    $uses = $style->get('uses');
    $expected = [
      'another_key' => 'another_key',
      'my_key' => '0',
      'third' => '0',
    ];
    $this->assertSame($expected, $uses);
  }

}
