<?php

namespace Drupal\Tests\style_entity\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Functional test(s) for style_entity uses.
 *
 * @group style_entity
 */
class StyleEntityUsesTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'style_entity_uses_test',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * Tests that field options are as EXPECTED.
   */
  public function testStyleEntityUses() {
    // Log in as admin.
    $this->drupalLogin($this->drupalCreateUser([], 'radmin', TRUE));

    $this->drupalGet('node/add/test_node_type');
    $this->assertSession()->pageTextContains('Create Test Node Type');

    $assert_session = $this->assertSession();
    // field_style uses the default selection handler.
    $assert_session->elementExists('css', '#edit-field-style-one');
    $assert_session->elementExists('css', '#edit-field-style-two');
    $assert_session->elementExists('css', '#edit-field-style-three');
    $assert_session->elementExists('css', '#edit-field-style-four');

    // The remaining fields all leverage the "uses" selection plugin.
    // These cases consider style entities with one, two, or no uses
    // along with fields that are configured to use one, two, or no uses
    // in an attempt to cover our bases.
    // field_style_even includes even uses.
    $assert_session->elementNotExists('css', '#edit-field-style-even-one');
    $assert_session->elementExists('css', '#edit-field-style-even-two');
    $assert_session->elementNotExists('css', '#edit-field-style-even-three');
    $assert_session->elementExists('css', '#edit-field-style-even-four');

    // field_style_prime includes prime uses.
    $assert_session->elementNotExists('css', '#edit-field-style-prime-one');
    $assert_session->elementExists('css', '#edit-field-style-prime-two');
    $assert_session->elementExists('css', '#edit-field-style-prime-three');
    $assert_session->elementNotExists('css', '#edit-field-style-prime-four');

    // field_style_both includes prime and even uses.
    $assert_session->elementNotExists('css', '#edit-field-style-both-one');
    $assert_session->elementExists('css', '#edit-field-style-both-two');
    $assert_session->elementExists('css', '#edit-field-style-both-three');
    $assert_session->elementExists('css', '#edit-field-style-both-four');

    // field_style_none includes neither prime nor even uses.
    $assert_session->elementNotExists('css', '#edit-field-style-none-one');
    $assert_session->elementNotExists('css', '#edit-field-style-none-two');
    $assert_session->elementNotExists('css', '#edit-field-style-none-three');
    $assert_session->elementNotExists('css', '#edit-field-style-none-four');
  }

}
