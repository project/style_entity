<?php

namespace Drupal\Tests\style_entity\Kernel;

use Drupal\KernelTests\KernelTestBase;
use Drupal\style_entity\Entity\Styles;

/**
 * Tests style_entity module.
 *
 * @group style_entity
 */
class StyleEntityTest extends KernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'style_entity',
  ];

  /**
   * Tests the static methods on Styles class.
   *
   * @dataProvider providersTestGetUses
   */
  public function testGetUses($configuration, $expected) {
    $config = \Drupal::configFactory()->getEditable('style_entity.uses');
    $config->set('uses', $configuration);
    $config->save();

    $uses = Styles::getUses();
    $this->assertSame($expected, $uses);
  }

  /**
   * DataProvider for testGetUses().
   *
   * @return array[]
   */
  public function providersTestGetUses() {
    return [
      'nothing' => [
        'configuration' => NULL,
        'expected' => [],
      ],
      'just_key' => [
        'configuration' => 'my_key',
        'expected' => [
          'my_key' => 'my_key',
        ],
      ],
      'key_label' => [
        'configuration' => 'my_key|My Label',
        'expected' => [
          'my_key' => 'My Label',
        ],
      ],
      'three_rows' => [
        'configuration' => "my_key|My Label\r\nanother\r\nthird|3",
        'expected' => [
          'my_key' => 'My Label',
          'another' => 'another',
          'third' => '3',
        ],
      ],
    ];
  }

}
